variable "prefix" {
  default = "pach"
}

variable "project" {
  default = "paranuara-challenge"
}

variable "contact" {
  default = "iascending@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "Ec2Instance"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "760637161969.dkr.ecr.ap-southeast-2.amazonaws.com/paranuara-challenge:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "760637161969.dkr.ecr.ap-southeast-2.amazonaws.com/web-app-api-nginx:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "lifeascending.net"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}