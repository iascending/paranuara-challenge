FROM python:3.9-rc-alpine3.11
LABEL maintainer="Jason LI"

ENV PYTHONUNBUFFERED 1
ENV PATH="/scripts:${PATH}"

RUN pip install --upgrade pip

COPY ./requirements.txt /requirements.txt
RUN apk add --update --no-cache postgresql-client jpeg-dev
RUN apk add --update --no-cache --virtual .tmp-build-deps \
    gcc libc-dev linux-headers postgresql-dev python3-dev musl-dev zlib zlib-dev && \
    pip install -r /requirements.txt

RUN mkdir /app
WORKDIR /app
COPY ./app /app

COPY ./scripts /scripts
RUN chmod +x /scripts/*

RUN adduser -D user
RUN mkdir -p /vol
RUN chown -R user:user /vol/
USER user
RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static

CMD ["entrypoint.sh"]
